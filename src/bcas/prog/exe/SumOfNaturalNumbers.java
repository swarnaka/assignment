 package bcas.prog.exe;

 //Java Program to find sum of N Natural Numbers

 import java.util.Scanner;

 public class SumOfNaturalNumbers {
	 private static Scanner scan;
		public static void main(String[]args) {
			 
			int number, i = 1, sum = 0;
			scan = new Scanner(System.in);
			
			System.out.print(" Please Enter any Number : ");
			number = scan.nextInt();	
			
			while(i <= number)
			{
				sum = sum + i; 
				i++;
			}	
			
			System.out.println("\n The Sum of Natural Numbers from 1 to "+ number + " = " + sum);
	}
	

}
