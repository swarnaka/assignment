 package bcas.prog.prod;

 public class ProceduralEx {
	public static void main(String[]args) {
		
		int hours = 1;
		int minutes = 0;
		int seconds = 0;

 while(true) {
    // 1. printing the time
	print( hours, minutes,  seconds) ;
    System.out.println();

    // 2. advancing the second hand
    seconds = seconds + 1;

    // 3. advancing the other hands when necessary
    if (seconds > 59) {
        minutes = minutes + 1;
        seconds = 0;

        if (minutes > 59) {
            hours = hours + 1;
            minutes = 0;

            if (hours > 23) {
                hours = 0;
            	}
        	}
    	}
 	}
 }


}
